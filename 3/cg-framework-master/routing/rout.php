<?php

use core\App;

App::$collector->get('/', ['workspace\controllers\MainController', 'actionIndex']);

App::$collector->get('/contacts', ['workspace\controllers\ContactsController', 'actionGet']);
App::$collector->post('/contacts', ['workspace\controllers\ContactsController', 'actionPost']);
App::$collector->post('/contacts-by-email', ['workspace\controllers\ContactsController', 'actionGetByEmail']);