<?php


namespace workspace\models;


use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    public $timestamps = false;
    protected $table = "contacts";
    protected $fillable = ['phone', 'email'];

}