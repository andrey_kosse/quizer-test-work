<?php
/* Smarty version 3.1.33, created on 2019-11-26 17:34:26
  from '/home/akosse/PhpstormProjects/cg-framework-master/workspace/views/layouts/main.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ddd37f28ef409_08563038',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9db106d62636d6b034cbf61ba74463ea9ac62002' => 
    array (
      0 => '/home/akosse/PhpstormProjects/cg-framework-master/workspace/views/layouts/main.tpl',
      1 => 1574778865,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ddd37f28ef409_08563038 (Smarty_Internal_Template $_smarty_tpl) {
?><html>
<?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['workspace_dir']->value)."/assets/resources.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
<head>
    <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'meta');?>

    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
    <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'css');?>

    <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'js_head');?>

</head>
<body>
<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

<div class="two-col">
    <div class="col1">
        <form id="post_contact">
            <fieldset>
                <legend>Add your phone number</legend>
                <H4>Option 1. Add your phone number</H4>
                Enter your PHONE: <br>
                <input type="text" name="phone" required="required"><br><br>
                Enter your e-mail *: <br>
                <input type="text" name="email" required="required"><br><br>
                <label>You will be able to retrieve your phone number later on using your e-mail</label><br><br>
                <input type="submit"> <br>
                <h3 id='post_result'></h3>
            </fieldset>
        </form>
    </div>
    <div class="col2">
        <form id="get_by_email">
            <fieldset>
                <legend>Retrieve your phone number</legend>
                <H4>Option 2. Retrieve your phone number</H4>
                Enter your e-mail *: <br>
                <input type="text" name="email" required="required"><br><br>
                <label>You will be able to retrieve your phone number later on using your e-mail</label><br><br>
                <input type="submit">
                <h3 id='get_result'></h3>
            </fieldset>
        </form>
    </div>
</div>
<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'js_body');?>

</body>
</html><?php }
}
