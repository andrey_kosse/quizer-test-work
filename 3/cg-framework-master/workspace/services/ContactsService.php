<?php

namespace workspace\services;

use Exception;
use Illuminate\Support\Facades\DB;
use workspace\models\Contacts;

class ContactsService
{

    public static function getAllContacts()
    {
        return Contacts::all();
    }

    public static function addNewContact($post)
    {
        $json = json_decode($post, true);
        if (isset($json['phone']) && isset($json['email'])) {
            $contact = new Contacts();
            $contact->phone = $json['phone'];
            $contact->email = $json['email'];
            try {
                $contact->save();
                return true;
            } catch (Exception $e) {
                return false;
            }
        } else return false;
    }

    public static function getContactByEmail($get)
    {
        $json = json_decode($get, true);
        if (isset($json['email']))
        {
            $list = Contacts::all()->where('email', 'like', $json['email']);
            return $list;

        } else return array();
    }
}