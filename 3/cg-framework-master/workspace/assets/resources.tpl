{capture name=css}
    <link href="/resources/css/style.css" rel="stylesheet" type="text/css">
{/capture}

{capture name=js_head}
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script src="/resources/js/script.js"></script>
{/capture}

{capture name=js_body}

{/capture}

{capture name=meta}
    <meta charset="UTF-8">
{/capture}
