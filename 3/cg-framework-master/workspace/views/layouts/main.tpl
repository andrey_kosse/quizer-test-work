<html>
{include file="{$workspace_dir}/assets/resources.tpl"}
<head>
    {$smarty.capture.meta}
    <title>{$title}</title>
    {$smarty.capture.css}
    {$smarty.capture.js_head}
</head>
<body>
{$content}
<div class="two-col">
    <div class="col1">
        <form id="post_contact">
            <fieldset>
                <legend>Add your phone number</legend>
                <H4>Option 1. Add your phone number</H4>
                Enter your PHONE: <br>
                <input type="text" name="phone" required="required"><br><br>
                Enter your e-mail *: <br>
                <input type="text" name="email" required="required"><br><br>
                <label>You will be able to retrieve your phone number later on using your e-mail</label><br><br>
                <input type="submit"> <br>
                <h3 id='post_result'></h3>
            </fieldset>
        </form>
    </div>
    <div class="col2">
        <form id="get_by_email">
            <fieldset>
                <legend>Retrieve your phone number</legend>
                <H4>Option 2. Retrieve your phone number</H4>
                Enter your e-mail *: <br>
                <input type="text" name="email" required="required"><br><br>
                <label>You will be able to retrieve your phone number later on using your e-mail</label><br><br>
                <input type="submit">
                <h3 id='get_result'></h3>
            </fieldset>
        </form>
    </div>
</div>
{$smarty.capture.js_body}
</body>
</html>