<?php

namespace workspace\controllers;


use core\App;
use core\Controller;
use core\ResponseType;
use workspace\services\ContactsService;

class ContactsController extends Controller
{

    public function actionPost()
    {
        App::$responseType = ResponseType::APPLICATION_JSON;
        App::$header->add('Access-Control-Allow-Origin', '*');

        $post = file_get_contents('php://input');
        if ($post !== '') {
            return json_encode(ContactsService::addNewContact($post));
        } else return json_encode(false);
    }

    public function actionGetByEmail()
    {
        App::$responseType = ResponseType::APPLICATION_JSON;
        App::$header->add('Access-Control-Allow-Origin', '*');

        $post = file_get_contents('php://input');
        return json_encode(ContactsService::getContactByEmail($post));
    }

    public function actionGet()
    {
        App::$responseType = ResponseType::APPLICATION_JSON;
        App::$header->add('Access-Control-Allow-Origin', '*');

        return json_encode(ContactsService::getAllContacts());
    }
}