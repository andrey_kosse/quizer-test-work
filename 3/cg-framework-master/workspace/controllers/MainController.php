<?php

namespace workspace\controllers;

use core\App;
use core\Controller;

class MainController extends Controller
{

    public function actionIndex()
    {
        return $this->render('main/index.tpl', ['title' => 'Quizer', 'h1' => 'Quizer ' . App::$config['app_name']]);
    }

}