<?php

//настройки локальной базы
return [
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'user' => 'root',
        'pass' => 'password123',
        'db_name' => 'contacts',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
    ]
];
