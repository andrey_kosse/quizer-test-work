var request;

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    var req = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return req.test(phone);
}

$(document).ready(function () {

    var $postResult = $("#post_result");
    var $getResult = $("#get_result");

    $("#post_contact").submit(function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedDataArr = $form.serializeArray();
        $inputs.prop("disabled", true);

        var email = serializedDataArr[1].value;
        var phone = serializedDataArr[0].value;

        if (!validateEmail(email)) {
            $postResult.text(email + " is not valid email");
            $postResult.css("color", "red");
            $inputs.prop("disabled", false);
            return;
        }
        if (!validatePhone(phone)) {
            $postResult.text(phone + " is not valid phone");
            $postResult.css("color", "red");
            $inputs.prop("disabled", false);
            return;
        }

        var jsonString = JSON.stringify({
            'phone': phone,
            'email': email
        });

        $.ajax({
            url: '/contacts',
            type: "post",
            data: jsonString,
            success: function (response) {

                if (JSON.parse(response)) {
                    $postResult.text("Data is valid and saved");
                    $postResult.css("color", "green");
                } else {
                    $postResult.text("This phone already used");
                    $postResult.css("color", "red");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $postResult.text(textStatus);
                $postResult.css("color", "red");
                console.log(textStatus, errorThrown);
            }
        });
        $inputs.prop("disabled", false);
    });

    $("#get_by_email").submit(function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedDataArr = $form.serializeArray();
        $inputs.prop("disabled", true);

        var email = serializedDataArr[0].value;

        if (!validateEmail(email)) {
            $getResult.text(email + " is not valid email");
            $getResult.css("color", "red");
            $inputs.prop("disabled", false);
            return;
        }

        var jsonString = JSON.stringify({
            'email': email
        });
        console.log(email);
        console.log(jsonString);

        $.ajax({
            url: '/contacts-by-email',
            type: "post",
            data: jsonString,
            success: function (response) {
                if (response.length !== 0) {
                    var phoneStr = '';
                    $.each(response, function (key, value) {
                        phoneStr += value['phone'] + "\n";
                    });
                    $getResult.text(phoneStr);
                    $getResult.css("color", "green");
                } else {
                    $getResult.text("Not Found");
                    $getResult.css("color", "red");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $getResult.text(textStatus);
                $getResult.css("color", "red");
                console.log(textStatus, errorThrown);
            }
        });
        $inputs.prop("disabled", false);
    });
});