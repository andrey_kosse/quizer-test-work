<?php

/*
    Дан массив произвольного размера с целыми числами в пределах от 1 до 1,000,000.
    В этом массиве все числа уникальные, кроме одного числа, которое повторяется два раза.
    Найти это число.
    Решить эту задачу с минимальным использованием процессорного времени.
    Решить на PHP и выслать рабочий код.
*/

$size = 1000000;
$time_start = microtime(true);

try {
    $rand_pos = random_int(0, $size);
    $rand_elem = random_int(0, $size);
} catch (Exception $e) {
    var_dump($e);
    die();
}

$rand_num_arr = range(0, $size);
shuffle($rand_num_arr );

echo "Random repeating position: " . $rand_pos  ."\n";
echo "Random repeating element: " . $rand_elem  ."\n";

$rand_num_arr[$rand_pos] = $rand_elem;

//print_r($rand_num_arr);

findDuplicates($rand_num_arr, $size);

echo 'Total execution time in seconds: '. (microtime(true) - $time_start);

/**
 * find duplicates in O(n)
 * time and O(1) extra space | Set 1
 * @param int $size
 * @param array $rand_num_arr
 */
function findDuplicates(array $rand_num_arr, int $size)
{
    echo "The repeating elements are: ";
    for ($i = 0; $i < $size; $i++) {
        if ($rand_num_arr[abs($rand_num_arr[$i])] >= 0)
            $rand_num_arr[abs($rand_num_arr[$i])] *= -1;
        else
            echo abs($rand_num_arr[$i]) . ' at position: ' . $i . "\n";
    }
}